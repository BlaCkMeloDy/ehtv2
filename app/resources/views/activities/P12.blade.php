@extends('activities.layout.activityLayout')

@section('header-more')

<link rel="stylesheet" href="{{ asset('css/screens/p12.css') }}">
<style>
	body {
		background: url({{ asset('img/stave2.jpg') }}) no-repeat center bottom fixed;
		background-size: cover;
	}
	* {
  outline:none;
	border:none;
	margin:0px;
	padding:0px;
}
#col-md-7 {
	background:#333 url(https://static.tumblr.com/maopbtg/a5emgtoju/inflicted.png) repeat;        
}
#paper {
	color:#FFF;
	font-size:20px;
}
#margin {
	margin-left:12px;
	margin-bottom:20px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none; 
}
#text {
	width: 40vw;
	overflow:hidden;
	background-color:#FFF;
	color:#222;
	font-weight:normal;
	font-size:24px;
	resize:none;
	line-height:40px;
	padding-left:100px;
	padding-right:100px;
	padding-top:45px;
	padding-bottom:34px;
	background-image:url(https://static.tumblr.com/maopbtg/E9Bmgtoht/lines.png), url(https://static.tumblr.com/maopbtg/nBUmgtogx/paper.png);
	background-repeat:repeat-y, repeat;
	-webkit-border-radius:12px;
	border-radius:12px;
	-webkit-box-shadow: 0px 2px 14px #000;
	box-shadow: 0px 2px 14px #000;
	border-top:1px solid #FFF;
	border-bottom:1px solid #FFF;
}
#title {
	background-color:transparent;
	border-bottom:3px solid #FFF;
	color:#FFF;
	font-size:20px;
	height:28px;
	font-weight:bold;
	width:220px;
}
#button {
	cursor:pointer;
	margin-top:20px;
	float:right;
	height:40px;
	padding-left:24px;
	padding-right:24px;
	font-weight:bold;
	font-size:20px;
	color:#FFF;
	text-shadow: 0px -1px 0px #000000;
	-webkit-border-radius:8px;
	border-radius:8px;
	border-top:1px solid #FFF;
	-webkit-box-shadow: 0px 2px 14px #000;
	box-shadow: 0px 2px 14px #000;
	background-color: #62add6;
	background-image:url(https://static.tumblr.com/maopbtg/ZHLmgtok7/button.png);
	background-repeat:repeat-x;
}
#button:active {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
#button:focus {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
#wrapper {
	width:700px;
	height:auto;
	margin-left:auto;
	margin-right:auto;
	margin-top:24px;
	margin-bottom:100px;
}
</style>

@stop

@section('actContent')

<div>
	<audio id="sample"></audio>
	<audio id="auRecord"></audio>
</div>

<div class="row debai">
Tương tác nhóm
</div>

<div class="col-md-7" style="margin-top: 2%;">
<div id="wrapper">

<form id="paper" method="get" action="">
	<textarea placeholder="Enter something funny." id="text" name="text" rows="20" style="overflow: hidden; word-wrap: break-word; resize: none; height: 70vh; "></textarea>  
	<br>
</form>

</div>
</div>

<div class="replay">
	<img class="fillable" src="{{ asset('img/testAnimate/replay.png') }}" id="playSample">
</div>

<div class="record">
	<img class="fillable" src="{{ asset('img/testAnimate/record.png') }}" id="record">
</div>

<div id="alert">
	To record your voice, first click on the record sign at the middle.
</div>

<script>
	$('.replay').click(function() {
		playRecord();
	});

	$('.record').click(function() {
		startRecording(this);
	});

	$('.wordWrap2').click(function() {
		switchLanguage(this);
	});

	TweenMax.from('.replay', 1, {scale:0.5, y:300, delay:1, ease:Elastic.easeOut});
	TweenMax.from('.record', 1, {scale:0.5, y:300, delay:1.3, ease:Elastic.easeOut});

	setTimeout(function() {
		$('.wordWrap2.vietnamese').addClass('animated flipInY').show().one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function play() {
			$('.wordWrap2.vietnamese').css('display', 'inline-block');
			$('.wordWrap2.vietnamese').off('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');

			$('.wordWrap2').click(function() {
				switchLanguage(this);
			});

			$('.wordWrap2')
			.mouseenter(function() {
				$(this).removeClass('flipInY pulse').addClass('pulse infinite');
			})
			.mouseleave(function() {
				$(this).removeClass('infinite');
			});
		});
	}, 1500);

</script>

<script>
	var audio_context;
	var recorder;
	var tl;
	var assetPath = '{{ asset('') }}';

	var busyPlay, busyReplay, busyRecord, 
		enabledPlay, enabledReplay, enabledRecord, 
		disabledPlay, disabledReplay, disabledRecord;
	function preloadImage() {
		busyReplay = new Image();
		busyReplay.src = '{{ asset('img/testAnimate/replay-blu.png') }}';
		busyRecord = new Image();
		busyRecord.src = '{{ asset('img/P7/pause.png') }}';
		enabledReplay = new Image();
		enabledReplay.src = '{{ asset('img/testAnimate/replay.png') }}';
		enabledRecord = new Image();
		enabledRecord.src = '{{ asset('img/testAnimate/record.png') }}';
		disabledReplay = new Image();
		disabledReplay.src = '{{ asset('img/testAnimate/replay-red.png') }}';
		disabledRecord = new Image();
		disabledRecord.src = '{{ asset('img/testAnimate/record-red.png') }}';
	}

	function startUserMedia(stream) {
		var input = audio_context.createMediaStreamSource(stream);

		recorder = new Recorder(input);
	}

	window.onload = function init() {
		try {
			// webkit shim
			window.AudioContext = window.AudioContext || window.webkitAudioContext;
			navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);
			window.URL = window.URL || window.webkitURL;

			audio_context = new AudioContext;
		} catch (e) {
			alert('No web audio support in this browser!');
		}

		navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
			window.alert('No live audio input: ' + e);
		});

		$(document).ready(function(){
			var elementData = <?php echo json_encode($elementData); ?>;
			document.getElementById('sample').setAttribute('src', '{{ asset('') }}' + elementData[0].audio);
			document.getElementById('sample').load();

			preloadImage();
		});
	};

	$(document).ready(function(){
  $('#title').focus();
    $('#text').autosize();
});
</script>

<script src="{{ asset('js/screens/p12.js') }}"></script>
<script src="{{ asset('js/recorder.js') }}"></script>

@stop

@section('actDescription-vi')
	Sau khi chuẩn bị xong, hãy ghi âm và nghe lại bài giới thiệu của mình.
@stop

@section('actDescription-en')
	Record and playback your introduction after your preparation.
@stop