@extends('layouts.app')

@section('header')

<link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/KiraNav.css') }}">

<style>
    .btn-group {
        display: initial;
    }

    .panel-heading {
        padding: 12px 15px;
    }

    .panel-heading a {
        font-size: 1.1em !important;
    }

    .unavailable {
        background-color: #d5eef6;
        border-color: #d5eef6;
        font-style: italic;
        pointer-events: none;
    }

    #promo1 {
        background: url({{ asset('img/banner-default.jpg')
    }
    }

    );
    }
</style>

@stop

@section('body')

<div id="promo1">

    <div class="grey-section">
        <div class="container site-section" id="descript">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h1>PHƯƠNG PHÁP KHAI THÁC GIÁO TRÌNH EM HỌC TIẾNG VIỆT TRÊN MÁY TÍNH</h1>

                    <div style="text-align: left; font-size: 20px;">
                        CÀI ĐẶT
                        <br><br>- Các em cài đặt phần mềm Chương trình vào máy tính của mình.
                        <br><br>- Mỗi lần học, các em truy cập vào phần mềm này và mở Chương trình ra.
                        <br><br>- Ở những lần học đầu tiên, các em mở phần “Giới thiệu Giáo trình” và đọc kỹ
                        <br><br>- Các em lựa chọn phần mình muốn học và kích hoạt.
                        <br><br>LỰA CHỌN PHẦN HỌC
                        <br><br>- Giáo trình bao gồm các hoạt động khác nhau, các em có thể tùy thích mở phần nào mà mình đang quan tâm để vào học tập, ôn luyện.
                        <br><br>- Một bài học sẽ có 2 phần chính: phần các tình huống ban đầu và phần các hoạt động tiếp thu và luyện tập. Phần tình huống gồm từ 4-8 tình huống tùy theo từng bài. Phần hoạt động tiếp thu và luyện tập bao gồm 15 hoạt động.
                        <br><br>- Thông thường, các em mở bài học mới, lần lượt thực hiện các hoạt động từ đầu đến cuối. Các em nên nhớ có thể chuyển từ phần này qua phần kia bất cứ lúc nào, chẳng hạn khi mình mong muốn xem lại những gì mình quên hoặc không chắc chắn.
                        <br><br>THỰC HIỆN CÁC PHẦN
                        <br><br>Phần “Tình huống”
                        <br><br>- Trong phần tình huống, các tình huống đưa ra nêu chủ đề của bài học, các từ và câu liên quan đến chủ đề bài học, thông thường đó là các bài hội thoại, hoặc các đoạn viết ngắn.
                        <br><br>- Các em nhấp chuột vào từng tình huống S, tình huống sẽ hiện ra, các em nhấp chuột vào nút khởi động để bắt đầu nghe. Các em có thể nghe đi nghe lại nhiều lần, mỗi lần như vậy chỉ cần nhấp chuột vào phần S mà mình muốn nghe.
                        <br><br>- Về mặt nghĩa của các câu trong tình huống, sau mỗi câu đều có câu bằng tiếng nước ngoài tương ứng.
                        <br><br>- Các em nghe, chú ý cách phát âm (đặc biệt là các thanh điệu), cách thể hiện ngữ điệu và có thể bắt trước nói theo. Các em nghe và luyện nói nhiều lần theo từng tình huống, cho đến khi phát âm các câu trôi chảy nhất.
                        <br><br>- Yêu cầu của phần này là làm quen với chủ đề, nghe và luyện cách phát âm, hiểu ý nghĩa chung của các hội thoại, chưa cần luyện tập từ, luyện tập câu, luyện tập đoạn thoại hay luyện tập cả bài hội thoại.
                        <br><br>- Sau các tình huống, trong phần này còn có phần tập đếm. Các em nghe, ghi nhớ và nhắc lại.
                        <br><br>Phần “Thực hành ghi nhớ và giao tiếp”
                        <br><br>- Phần “Thực hành ghi nhớ và giao tiếp” bao gồm 15 hoạt động, được cấu trúc như sau:
                        <br><br>+ Hoạt động 1 và 2 là các hoạt động liên quan đến giới thiệu từ và ôn luyện về từ
                        <br><br>+ Hoạt động 3 và 4 là các hoạt động liên quan đến giới thiệu câu và ôn luyện về câu
                        <br><br>+ Hoạt động 5 và 6 là các hoạt động liên quan đến giới thiệu đoạn thoại và ôn luyện về đoạn thoại
                        <br><br>+ Hoạt động 7 và 8-9-10-11 là các hoạt động liên quan đến giới thiệu hội thoại và ôn luyện về hội thoại
                        <br><br>+ Hoạt động 12 là tương tác nhóm (trong trường hợp các em học thành lớp học)
                        <br><br>+ Hoạt động 13 là hoạt động học thuộc bài viết cho sẵn và thực hành viết bài của mình
                        <br><br>+ Hoạt động 14 là phần tóm tắt lại các mẫu câu chính có trong toàn bộ bài
                        <br><br>+ Hoạt động 15 là phần dành giới thiệu các yếu tố văn hóa gắn với ngôn ngữ. Ở đây có các hình ảnh về đất nước con người Việt nam, có những bài hát, bài thơ, thành ngữ tục ngữ ca dao, các câu đố vui và các trò chơi dân gian.
                        <br><br>- Ở các hoạt động 1-3-5, các em kích chuột vào từng từ, từng câu, từng đoạn thoại để nghe và nhắc lại theo mẫu. Các em nên nghe đi nghe lại nhiều lần, cố gắng bắt trước cách phát âm mẫu, sau đó tập phát âm, tập nói lại. Khi đã phát âm hay nói khá thành thục rồi, các em hãy ghi âm lại giọng nói của mình, nghe lại, đối chiếu với cách nói mẫu, chỉnh sửa rồi lại ghi lại, nghe lại..., cho đến khi mình thấy ưng ý. Về mặt ngữ nghĩa, mỗi khi nhấp chuột vào từ, vào câu hay vào đoạn thoại, đều có các từ, câu, đoạn thoại bằng tiếng nước ngoài tương ứng hiện ra cùng.
                        <br><br>Các hoạt động 2-4-6 là các hoạt động luyện tập tương ứng với các hoạt động 1-3-5. Ở các bài tập này, yêu cầu chính là nghe và tìm ra yếu tố đúng (từ đúng, câu đúng, đáp án đúng). Các em cần lựa chọn trong số nhiều yếu tố đưa ra yếu tố đúng với yếu tố vừa được nghe và nhấp chuột vào đấy. Đây là các bài tập khó nên các em phải cố gắng luyện tập nhiều lần. Khi các em làm đúng sẽ có hình và âm thanh khen các em, khi các em làm chưa đúng cũng có hình và âm thanh chỉ dẫn để các em làm lại. Hoạt động tương tác này để các em biết mình đúng hay sai và máy sẽ tính điểm để các em biết mức tiến bộ của mình.
                        <br><br>- Ở hoạt động 7 và các hoạt động 8-9-10-11, đây là các hoạt động liên quan đến giao tiếp hội thoại, là mục tiêu cần đạt đến khi học một ngôn ngữ.
                        <br><br>Ở hoạt động 7, các em kích chuột vào từng hội thoại, nghe cả bài hội thoại, đến từng câu thì cố gắng nhắc lại, nhớ bắt chước ngữ điệu giao tiếp của câu, cách nói của các nhân vật trong hội thoại. Nên nghe đi nghe lại hội thoại và nhắc lại nhiều lần. Về nghĩa, các câu trong hội thoại đều được đi kèm câu tiếng nước ngoài tương ứng.
                        <br><br>Các hoạt động 8-9-10-11 là các hoạt động luyện tập giao tiếp hội thoại, các dạng bài tập ở đây khá đa dạng, hoạt động 8 là sử dụng các từ cho sẵn để điền vào chố trống trong bài hội thoại, hoạt động 9 là sử dụng các câu cho sẵn để điền vào các câu còn thiếu trong bài hội thoại, các hoạt động này được tính thời gian thực hiện, đồng hồ đếm ngược cho biết thời gian 2 phút dành cho bài tập còn lại là bao nhiêu. Hoạt động 10 liên quan đến cấu trúc câu: các em sẽ sắp xếp lại các từ cho sẵn của một câu đã bị tráo thứ tự để thành một câu đúng, có nghĩa. Ở hoạt động 11, các em sẽ sắp xếp lại các câu cho trước thành một bài hội thoại hoàn chỉnh, các câu này thuộc một bài hội thoại nhưng đã bị tráo thứ tự.
                        <br><br>- Hoạt động 12 là hoạt động tương tác nhóm. Các em được giao một nhiệm vụ và cùng nhau chia sẻ công việc để hoàn thành nhiệm vụ đó. Nếu các em tự học một mình thì cũng có thể tự mình hoàn thành các nhiệm vụ này như khi đang ở trong nhóm học.
                        <br><br>Ở hoạt động 13, các em sẽ được giao một bài đọc. Các em sẽ tìm hiểu ý nghĩa chung của bài đọc qua phần bài dịch tương ứng và được yêu cầu học thuộc lòng bài đọc. Sau đó các em sẽ viết tự do về chủ đề đưa ra và ghi bài viết đó vào máy, như là kết quả học tập đã đạt được.
                        <br><br>Hoạt động 14 là bảng tóm tắt các mẫu câu, các cấu trúc câu chính có trong cả bài. Các em hãy ghi nhớ các mẫu câu này và từ đó đặt được nhiều các câu khác tương tự.
                        <br><br>- Phần cuối cùng của bài (hoạt động 15) là các hoạt động mở rộng. Trong phần này các em được tiếp xúc với các yếu tố văn hóa Việt Nam: đó là các hình ảnh về đất nước con người, là các bài hát và bài thơ dành cho lứa tuổi của các em, là những câu thành ngữ, tục ngữ, ca dao phản ánh phong tục tập quán của người Việt, là những câu đố vui và những trò chơi dân gian của trẻ em Việt nam. Qua tiếp xúc với các học liệu này, các em sẽ được mở rộng hiểu biết của mình về đất nước Việt nam và có được khả năng đọc thơ, hát và chơi những trò chơi của các bạn nhỏ ở Việt nam.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('vendor.footer')

@stop