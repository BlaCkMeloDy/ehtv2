@extends('layouts.app')

@section('header')

<link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/KiraNav.css') }}">

<style>
    .btn-group {
        display: initial;
    }

    .panel-heading {
        padding: 12px 15px;
    }

    .panel-heading a {
        font-size: 1.1em !important;
    }

    .unavailable {
        background-color: #d5eef6;
        border-color: #d5eef6;
        font-style: italic;
        pointer-events: none;
    }

    #promo1 {
        background: url({{ asset('img/banner-default.jpg')
    }
    }

    );
    }
</style>

@stop

@section('body')

<div id="promo1">

    <div class="grey-section">
        <div class="container site-section" id="descript">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h1>EM HỌC TIẾNG VIỆT</h1>

                    <div style="text-align: left; font-size: 20px;">
                        Các em thân mến,
                        <br>
                        <br>
                        Giáo trình EM HỌC TIẾNG VIỆT được biên soạn dành cho thanh thiếu nhi người Việt sống ở nước ngoài. Đây là một giáo trình dạy và học tiếng Việt theo hình thức trực tuyến (online), giúp người học tự học là chính, tuy nhiên giáo trình cũng hoàn toàn có thể được tổ chức giảng dạy theo lớp học trực tiếp. Giáo trình được biên soạn song ngữ, gồm 11 phiên bản cho cộng đồng kiều bào ở các nước sở tại nói các thứ tiếng Anh, Lào, Thái, Campuchia, Pháp, Trung Quốc, Nga, Đức, Nhật, Hàn và Ả Rập.
                        <br>
                        <br>
                        Giáo trình EM HỌC TIẾNG VIỆT được biên soạn theo đường hướng giao tiếp-hành động, lấy trọng tâm là thực hành tiếng Việt trong các tình huống gần gũi đối với người học nhỏ tuổi, trước hết là trong gia đình, ở trường học và sau nữa là ngoài xã hội. Chủ đề, từ vựng và các kiến thức ngôn ngữ được lựa chọn cẩn thận, phù hợp với lứa tuổi và tạo được hứng thú cho các em. Là giáo trình song ngữ, các em luôn được hỗ trợ trong việc tìm hiểu ngữ nghĩa của các từ, các cụm từ, các câu, các yếu tố văn hóa, văn minh, các thói quen và phong tục tập quán, các hiểu biết về đất nước và con người Việt nam.
                        <br>
                        <br>
                        Giáo trình gồm 20 bài, trong đó có 16 bài học và 4 bài ôn tập. Mỗi bài học bao gồm 15 phần, giới thiệu các tình huống, từ vựng, cấu trúc câu, các giải thích cụ thể về những sự tương đồng và khác biệt về ngôn ngữ cũng như về văn hóa giữa hai thứ tiếng và hai nền văn hóa, các hoạt động đa dạng để giúp người học luyện tập ghi nhớ và thực hành tiếng Việt. Cuối mỗi bài có các bài hát, bài đồng giao, bài thơ, thành ngữ tục ngữ ca dao, câu đố vui và các trò chơi truyền thống của trẻ em Việt Nam. Cuối cùng là một số hình ảnh lựa chọn để phản ánh phần nào về đất nước và con người Việt nam.
                        <br>
                        <br>
                        Là giáo trình trực tuyến, các em có thể tiếp cận và tự học mọi nơi, mọi lúc, trên mọi thiết bị như máy tính, máy tính bảng, điện thoại thông minh. Nhờ tính tương tác cao, các em có thể thực hành luyện tập ngay trên máy, được nghe âm chuẩn, được ghi giọng nói của mình và nghe lại những gì mình thực hành, trực tiếp sửa lỗi và chỉnh sửa mọi hoạt động trên máy, được biểu đạt viết và ghi lại bài viết của mình để so sánh với các bài viết mẫu. Thiết kế giáo trình cho phép các em kích chuột để liên tục nghe đi nghe lại từng từ, từng câu, từng mẩu hội thoại và từng tình huống hội thoại theo như các em mong muốn.
                        <br>
                        <br>
                        Về hình thức, giáo trình được trình bày phù hợp với tâm lý và thẩm mỹ của các em, với các minh họa theo phong cách hoạt hình, từ nhân vật đến khung cảnh, tình huống, đến cách thức giới thiệu ngữ liệu, cách đặt từ ngữ song ngữ và cách thông báo kết quả thực hành ngôn ngữ, với lời khen động viên đi kèm âm thanh, hình ảnh.
                        <br>
                        <br>
                        Hy vọng giáo trình trực tuyến EM HỌC TIẾNG VIỆT sẽ mang đến cho các em một công cụ mới, một cơ hội mới, giúp các em trau dồi tiếng Việt một cách thuận lợi, dễ dàng, sử dụng tiếng Việt trong cuộc sống gia đình, trong học tập và trong mỗi chuyến trở về thăm quê hương đất nước.
                        <br>
                        <br>
                        Chúc các em thành công!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('vendor.footer')

@stop