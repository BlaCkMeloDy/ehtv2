@extends('layouts.app')

@section('header')

<link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/KiraNav.css') }}">
<link rel="stylesheet" href="{{ asset('css/screens/introduce.css') }}">
@stop

@section('body')

<div id="promo1">
    <div class="jumbotron about">
        <div class="container">
            <div class="row about">
                <div class="col-md-6 about">
                    <h1>Giới thiệu</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="background-container">
    <div class="container padding-top-bot">
        <div class="row">
            <div class="col-lg-4">
                <a href="/">
                    <div>
                        <img class="full-width" src="https://media.sharegate.com/app/uploads/2018/11/article-becomingmore-cover.png">
                    </div>
                    <div class="posts-section--content p4 lg-p3 xl-p4 flex flex-stretch flex-auto">
                        <div class="flex flex-column w-full max-w-full">
                            <div class="flex-auto">
                                <h4 class="mb3 mt1 h2 lg-h4 xl-h3">Các bài hát</h4>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="/">
                    <div>
                        <img class="full-width" src="https://media.sharegate.com/app/uploads/2018/11/article-becomingmore-cover.png">
                    </div>
                    <div class="posts-section--content p4 lg-p3 xl-p4 flex flex-stretch flex-auto">
                        <div class="flex flex-column w-full max-w-full">
                            <div class="flex-auto">
                                <h4 class="mb3 mt1 h2 lg-h4 xl-h3">Các bài thơ</h4>
                            </div>
                        </div>

                    </div>
                </a>

            </div>
            <div class="col-lg-4">
                <a href="/">
                    <div>
                        <img class="full-width" src="https://media.sharegate.com/app/uploads/2018/11/article-becomingmore-cover.png">
                    </div>
                    <div class="posts-section--content p4 lg-p3 xl-p4 flex flex-stretch flex-auto">
                        <div class="flex flex-column w-full max-w-full">
                            <div class="flex-auto">
                                <h4 class="mb3 mt1 h2 lg-h4 xl-h3">Các hình ảnh văn hóa</h4>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="/">
                    <div>
                        <img class="full-width" src="https://media.sharegate.com/app/uploads/2018/11/article-becomingmore-cover.png">
                    </div>
                    <div class="posts-section--content p4 lg-p3 xl-p4 flex flex-stretch flex-auto">
                        <div class="flex flex-column w-full max-w-full">
                            <div class="flex-auto">
                                <h4 class="mb3 mt1 h2 lg-h4 xl-h3">Các trò chơi</h4>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="/">
                    <div>
                        <img class="full-width" src="https://media.sharegate.com/app/uploads/2018/11/article-becomingmore-cover.png">
                    </div>
                    <div class="posts-section--content p4 lg-p3 xl-p4 flex flex-stretch flex-auto">
                        <div class="flex flex-column w-full max-w-full">
                            <div class="flex-auto">
                                <h4 class="mb3 mt1 h2 lg-h4 xl-h3">Các clip khác</h4>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@include('vendor.footer')

@stop