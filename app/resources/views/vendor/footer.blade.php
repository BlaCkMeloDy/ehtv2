<footer>
    <div class="row">
        <div class="col-md-4 col-sm-6 footer-navigation">
            <h3><a href="#">Em Học Tiếng Việt </a></h3>
            <p class="company-name">Trường Đại học Ngoại ngữ</p>
        </div>
        <div class="col-md-4 col-sm-6 footer-contacts">
            <div><i class="fa fa-phone footer-contacts-icon"></i>
                <p class="footer-center-info email text-left">+84123456789</p>
            </div>
            <div><i class="fa fa-envelope footer-contacts-icon"></i>
                <p> <a href="#" target="_blank">support@kirakira.com</a></p>
            </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
    </div>
</div>
</footer>