@extends('layouts.app')
<link rel="stylesheet" href="{{ asset('css/screens/welcome.css') }}">
@section('body')

<style>
    .row.wehave {
        display: flex;
        align-items: center;
    }

    .row.wehave h3 {
        font-size: 2.5em;
    }

    .no-padding {
        padding-left: 0;
        padding-right: 0;
    }

    .row.wehave p {
        font-size: 1.5em;
    }

    .row.introduction {
        display: flex;
        align-items: center;
    }

    .teacher_img img {
        width: 600px;
        max-width: 100%;
        text-align: center;
    }

    @media screen and (max-width: 991px) {
        .row.wehave {
            display: block;
        }
    }

    @media screen and (max-width: 767px) {
        .row.introduction {
            display: block;
        }
    }
</style>
<!-- <img src="{{ asset('img/banner-default.jpg') }}" style="width: 100%;"> -->

<div id="promo">
    <div class="jumbotron hero">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-10 get-it">
                    <h1>Em Học Tiếng Việt</h1>
                    <p>Giáo trình EM HỌC TIẾNG VIỆT được biên soạn dành cho thanh thiếu nhi người Việt sống ở nước ngoài. Đây là một giáo trình dạy và học tiếng Việt theo hình thức trực tuyến (online), giúp người học tự học là chính, tuy nhiên giáo trình cũng hoàn toàn có thể được tổ chức giảng dạy theo lớp học trực tiếp. Giáo trình được biên soạn song ngữ, gồm 11 phiên bản cho cộng đồng kiều bào ở các nước sở tại nói các thứ tiếng Anh, Lào, Thái, Campuchia, Pháp, Trung Quốc, Nga, Đức, Nhật, Hàn và Ả Rập.... </p>
                    <p><a class="btn btn-primary margin-sm" role="button" href="{{route('introduce')}}">Giới thiệu<i class="fa fa-small fa-graduation-cap"></i></a></p>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="features">
    <div class="row more-content">
        <!-- <a href="{{route('introduce')}}">
            <div class="col-sm-offset-2 col-sm-2 feature rainbow"><img src="{{ asset('img/chat.svg') }}" align="center">
                <h3>Giới thiệu</h3>
            </div>
        </a> -->
        <!-- <a href="{{route('lessons')}}"> -->
        <div class="col-sm-12 feature">
            <div class="row">

                <div class="col-sm-4 rounded mx-auto d-block" align="center">
                    <img src="{{ asset('img/airplane.svg') }}">
                    <h3>Bài học</h3>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        @for($i = 1; $i <= 20; $i++) <a href="{{url('/')}}/lesson{{$i}}/situations" class="col-sm-3" style="padding: 3px;">
                            <div class="btn btn-success" style="border-radius: 50%; height: 50px; width: 50px; text-align: center;">{{$i}}</div>
                            </a>
                            @endfor
                    </div>

                </div>
            </div>

        </div>
        <!-- </a> -->
        <!-- <a href="{{route('materials')}}">
            <div class="col-sm-4 feature"><img src="{{ asset('img/profits.svg') }}">
                <h3>Học liệu</h3>
            </div>
        </a> -->
    </div>
</div>

<div class="container-fluid author">
    <h1 class="text-center">With content created by...</h1>
    <div class="row introduction">
        <div class="col-sm-5 col-sm-push-7 teacher_img">
            <div class="img_container"><img class="img-circle" src="{{ asset('img/nlt.jpg') }}"></div>
        </div>
        <div class="col-sm-7 col-sm-pull-5 author_info">
            <h2 class="author_info">Ph.D Lan Trung Nguyen</h2>
            <p class="author_info">Ph.D Lan Trung Nguyen, currently holding the position of Vice-principal of the University of Languages and International Studies, is well known for his vast knowledge of international languages as well as his enormous contribution to the work of preserving and developing the national language, Vietnamese.</p>
        </div>
    </div>
</div>

@include('vendor.footer')

@stop